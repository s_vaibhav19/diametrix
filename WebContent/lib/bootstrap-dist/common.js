function mobileNoValidations(mobileno) {
	if (/^\d{10}$/.test(mobileno)) {
		return true;
	} else {
		return false;
	}
}

function emailvalidations(emailstr) {
	if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(emailstr)) {
		return true;
	} else {
		return false;
	}
}

function validateAllFields(formId) {
	var inputele = document.forms[formId].getElementsByTagName("input");
	for (i = 0; i < inputele.length; i++) {
		if (inputele[i].type == "text") {

			if(!inputele[i].disabled){
			
			if (inputele[i].value.trim() == "") {
				inputele[i].focus();
				alert("returnig FALSE text value");
				return false;
			}
			
		}
		if (inputele[i].type == "password") {
			if (inputele[i].value.trim() == "") {
				alert("returnig FALSE password value");
				return false;
			}
		}
		}
	}
	return true;
}
function clearAction(formId) {
	var inputele = document.forms[formId].getElementsByTagName("input");
	for (i = 0; i < inputele.length; i++) {
		inputele[i].value = "";
	}
	$("#addbuttons").prop("disabled", false);
	$("#updateButton").prop("disabled", true);
}