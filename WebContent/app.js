
var homepage = angular.module('homepage', [ 'ngRoute', 'firebase' ,'chart.js','homepage.directives','duScroll']);

homepage.config(function($routeProvider) {
	$routeProvider.when('/home', {
		templateUrl : 'views/mainhomepage.html'
	})
	.when('/blogging', {
		templateUrl: "views/bloggs/blogging.html"
	})
	.when('/tracking',{
		templateUrl: "views/tracking/trackingpage.html"
	})
	.when('/advance',{
		templateUrl: "views/settings/advance.html"
	})
	.when('/profile',{
		templateUrl: "views/settings/profile.html"
	})
	.when('/dashboard',{
		templateUrl: "views/dashboard/dashboard.html"
	})
	.when('/schedule',{
		templateUrl: "views/schedular/scheduler.html"
	})
	.when('/postBlogs',{
		templateUrl: "views/bloggs/postblog.html"
	})
	.when('/dietplan',{
		templateUrl: "views/schedular/dietplan.html"
	})
	.when('/reminders',{
		templateUrl: "views/schedular/reminder.html"
	})
	.when('/reports',{
		templateUrl: "views/shared/rptsharing.html"
	})
	.otherwise({
		redirectTo : '/home'
	});
}).controller("logincontroller", [ '$scope', '$location','$firebaseAuth','$firebaseObject','$firebaseArray','$rootScope', function($scope, $location, $firebaseAuth, $firebaseObject,$firebaseArray,$rootScope) {

	var fireRef = new Firebase("https://diametrix.firebaseio.com/");
	$scope.authObj = $firebaseAuth(fireRef);
	
var authData = $scope.authObj.$getAuth();
	
	if (null == authData) {
	$scope.loginaction = function(event) {
		
		/*
		 * Below method is used to check for the email id and password for
		 * authentications this is the method provided by firebase
		 */
		$scope.authObj.$authWithPassword({
			  email: $scope.username,
			  password: $scope.password
			}).then(function(authData) {
			  
			var reminders = $firebaseArray(new Firebase("https://diametrix.firebaseio.com/" + authData.uid + "/reminders/"));
			reminders.$loaded().then(function() {
				angular.forEach(reminders, function(data) {
					$rootScope.title.push(data.desc);
				    $rootScope.start.push(data.remdate+"T"+data.remtime+":00");
				})
			});
			var alerts = $firebaseArray(new Firebase("https://diametrix.firebaseio.com/" + authData.uid + "/alerts/"));
			var days = [ 'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thrusday', 'Friday', 'Saturday', 'Sunday' ];
			var today = new Date().getDay();
			alerts.$loaded().then(function() {
				angular.forEach(alerts, function(data) {
					switch (today) {
				       case 0:
				    	   if(data.Sunday){
				    		  /* $rootScope.alerttime.push(data.alerttime);
				    		   $rootScope.alertsub.puch(data.subject);*/
				    		   $rootScope.alertObj.push({
				    			   subjects: data.subject,
				    			   times: data.alerttime
				    		   });
				    	   }
				           break;
				       case 1:
				    	   if(data.Monday){
				    		   /* $rootScope.alerttime.push(data.alerttime);
				    		   $rootScope.alertsub.puch(data.subject);*/
				    		   $rootScope.alertObj.push({
				    			   subjects: data.subject,
				    			   times: data.alerttime
				    		   });
				    	   }
				           break;
				       case 2:
				    	   if(data.Tuesday){
				    		   /* $rootScope.alerttime.push(data.alerttime);
				    		   $rootScope.alertsub.puch(data.subject);*/
				    		   $rootScope.alertObj.push({
				    			   subjects: data.subject,
				    			   times: data.alerttime
				    		   });
				    	   }
				           break;
				       case 3:
				    	   if(data.Wednesday){
				    		   /* $rootScope.alerttime.push(data.alerttime);
				    		   $rootScope.alertsub.puch(data.subject);*/
				    		   $rootScope.alertObj.push({
				    			   subjects: data.subject,
				    			   times: data.alerttime
				    		   });
				    	   }
				           break;
				       case 4:
				    	   if(data.Thursday){
				    		   /* $rootScope.alerttime.push(data.alerttime);
				    		   $rootScope.alertsub.puch(data.subject);*/
				    		   $rootScope.alertObj.push({
				    			   subjects: data.subject,
				    			   times: data.alerttime
				    		   });
				    	   }
				           break;
				       case 5:
				    	   if(data.Friday){
				    		   /* $rootScope.alerttime.push(data.alerttime);
				    		   $rootScope.alertsub.puch(data.subject);*/
				    		   $rootScope.alertObj.push({
				    			   subjects: data.subject,
				    			   times: data.alerttime
				    		   });
				    	   }
				           break;
				       case 6:
				    	   if(data.Saturday){
				    		   /* $rootScope.alerttime.push(data.alerttime);
				    		   $rootScope.alertsub.puch(data.subject);*/
				    		   $rootScope.alertObj.push({
				    			   subjects: data.subject,
				    			   times: data.alerttime
				    		   });
				    	   }
				           break;
				   }
					    
				})
				if($rootScope.alertObj.length==0){
					$rootScope.alertObj.push({
		    			   subjects: "No-Alerts Today",
		    			   times: "00:00"
		    		   });
				}
			});
			$('#myModal').modal('hide');
			  $location.path('/dashboard');
			}).catch(function(error) {
			 /* console.error("Authentication failed:", error); */
			  $scope.errormsg="Authentication Failed Please Try Again";
			});
	}
	/*
	 * Below function is used for Registering new user if success full then
	 * redirect to profile page for updating the informations
	 */
	$scope.signupAction = function(event) {

		var fname = $scope.fname;
		var lname = $scope.lname;
		var uname = $scope.uname;
		var email = $scope.emailid;
		var password = $scope.password;
			/*
			 * Below Method is used for user creations and creating basic
			 * profile of user in fire base
			 */
		$scope.authObj.$createUser({
			  email: $scope.emailid,
			  password: $scope.createpassword
			}).then(function(userData) {
			  console.log("User " + userData.uid + " created successfully!");
			 
			  /* Creating Basic Profile under userid */
			 
			  var fireRefobj = new Firebase("https://diametrix.firebaseio.com/"+userData.uid+"/profile/");
			  
			  var fbobj = $firebaseObject(fireRefobj); 
			  fbobj.fname = fname;
			  fbobj.lname = lname;
			  fbobj.dob = "";
			  fbobj.maritalStatus = "";
			  fbobj.height = "";
			  fbobj.weight = "";
			  fbobj.sex = "";
			  fbobj.bloodgrp = "";
			  fbobj.rptShare = "";
			  fbobj.country = "";
			  fbobj.mobileno = "";
			  
				fbobj.$save().then(function(ref) {
					console.log(ref);

				}, function(error) {
					console.log("Error:", error);
				});
				/* Setting the default advance setting for the user */
				 var fbbobj = $firebaseObject(new Firebase("https://diametrix.firebaseio.com/"+userData.uid+"/advsetting/")); 
				 fbbobj.BloodPressure=true;
				 fbbobj.Insulin=true;
				 fbbobj.SugarCount=true;
				 fbbobj.SleepingHrs=true;
				 fbbobj.ExcersixeHrs=true;
				 fbbobj.EyeCheckup=true;
				 fbbobj.FootCheckup=true;
				 fbbobj.$save().then(function(ref) {
						console.log(ref);

					}, function(error) {
						console.log("Error:", error);
					});
			  
			  return $scope.authObj.$authWithPassword({
			    email: $scope.emailid,
			    password: $scope.createpassword
			  });
			}).then(function(authData) {
				$('#signupmodal').modal('hide');
			  $location.path('/tracking');
			}).catch(function(error) {
				$scope.errormsgsignup="Error Please Try Again";
			});
		
	}
	$scope.forgotAction = function(event) {
		/*
		 * Below code is used to send email for reseting the password this is
		 * the default mechanism of firebase
		 */
		fireRef.resetPassword({
		  email: $scope.forgotpsd
		}, function(error) {
		  if (error) {
		    switch (error.code) {
		      case "INVALID_USER":
		        $scope.errorForgot="Invalid Credentials";
		        break;
		      default:
		    	  $scope.errorForgot="Invalid Credentials";
		    }
		  } else {
			  $scope.succForgot="Password reset email sent successfully!";
		  }
		});
	}
	}else{
		$location.path('/dashboard');
	}
} ]).controller('MyCtrl', function($scope, $document){
	console.log("inside");
    $scope.toTheTop = function() {
        $document.scrollTopAnimated(0, 5000).then(function() {
          console && console.log('You just scrolled to the top!');
        });
      }
      var section3 = angular.element(document.getElementById('product'));
      $scope.toSection3 = function() {
        $document.scrollToElementAnimated(section3);
      }
    }
  ).value('duScrollOffset', 30);


angular.module('homepage.directives', [])
.directive('validationCheck', ['$parse', function ($parse) {
    return {
      require: 'ngModel',
      link: function (scope, elm, attrs, ngModel) {
        var check = $parse(attrs.validationCheck);

        // Watch for changes to this input
        scope.$watch(check, function (newValue) {
          ngModel.$setValidity(attrs.name, newValue);
        });
      }
  }
}
]).run(function ($rootScope) {
    $rootScope.title = [];
    $rootScope.start = [];
    $rootScope.alertObj =[];
    
});

homepage.controller('logoutCtrl',['$scope','$location','$firebaseAuth','$firebaseObject','$rootScope', function($scope, $location, $firebaseAuth, $firebaseObject,$rootScope) {
// common logout functionality for all the pages
	$scope.logoutUser= function(){
		
		$scope.authObj = $firebaseAuth(new Firebase("https://diametrix.firebaseio.com/"));
		$scope.authObj.$unauth();
		
		$rootScope.title.splice(0,$rootScope.title.length);
	    $rootScope.start.splice(0,$rootScope.start.length);
	    $rootScope.alertObj.splice(0,$rootScope.alertObj.length);
		
	    $location.path('/home');
	}
}]);
