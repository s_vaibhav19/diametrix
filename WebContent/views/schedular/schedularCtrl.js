// Below code is used to update the Week Diet Plan and has 3 Way data binding
// Has on click function for fetching day data
homepage.controller('dietplanctrl', [
		'$scope',
		'$location',
		'$firebaseAuth',
		'$firebaseObject',
		'$firebaseArray',
		'$rootScope',
		function($scope, $location, $firebaseAuth, $firebaseObject, $firebaseArray, $rootScope) {

			var d = new Date();
			var n = d.getDay();
			var days = [ 'sunday', 'monday', 'tuesday', 'wednesday', 'thrusday', 'friday', 'saturday', 'sunday' ];
			var fireRef = new Firebase("https://diametrix.firebaseio.com/");
			$scope.authObj = $firebaseAuth(fireRef);
			var authData = $scope.authObj.$getAuth();
			if (null != authData) {
				$scope.dietPlan = $firebaseObject(new Firebase("https://diametrix.firebaseio.com/" + authData.uid + "/dietplan/" + days[n] + "/"));
				$scope.alerts = $firebaseObject(new Firebase("https://diametrix.firebaseio.com/" + authData.uid + "/alerts/"));
				document.getElementById(n).style.backgroundColor = "rgb(255, 195, 0)";
				
				$scope.changeDiet = function(day) {
					$scope.dietPlan = $firebaseObject(new Firebase("https://diametrix.firebaseio.com/" + authData.uid + "/dietplan/" + days[day]
							+ "/"));
					for (var i = 0; i < 7; i++) {
						document.getElementById(i).style.backgroundColor = "gray";
					}
					document.getElementById(day).style.backgroundColor = "rgb(255, 195, 0)";
				}
				$("#calendar").fullCalendar({
					header : {
						left : 'prev,next today',
						center : 'title',
						right : 'month,basicWeek,basicDay'
					},
					eventLimit : true, // allow "more" link when too many
					// events
					events : function(start, end, timezone, callback) {
						var events = [];
						for (var i = 0; i < $rootScope.title.length; i++) {
							events.push({
								title : $rootScope.title[i],
								start : $rootScope.start[i],
								textColor: "white"
							});
						}
						callback(events);
					}
				});
			} else {
				$location.path('/home');
			}
		} ]);

// Below function is used for CRUD operation for alerts functionality
homepage.controller('alertsCtrl', [ '$scope', '$location', '$firebaseAuth', '$firebaseObject', '$firebaseArray','$rootScope',
		function($scope, $location, $firebaseAuth, $firebaseObject, $firebaseArray,$rootScope) {
			var fireRef = new Firebase("https://diametrix.firebaseio.com/");
			$scope.authObj = $firebaseAuth(fireRef);
			var authData = $scope.authObj.$getAuth();
			if (null != authData) {
				$scope.alerts = $firebaseArray(new Firebase("https://diametrix.firebaseio.com/" + authData.uid + "/alerts/"));
				$scope.alertObj = {
					Monday : true,
					Tuesday : true,
					Wednesday : true,
					Thursday : true,
					Friday : true,
					Saturday : true,
					Sunday : true
				};
				$scope.addAlert = function(alertObj) {
					$scope.alerts.$add({
						subject : $scope.subject,
						alerttime : document.getElementById("alerttime").value,
						Monday : alertObj.Monday,
						Tuesday : alertObj.Tuesday,
						Wednesday : alertObj.Wednesday,
						Thursday : alertObj.Thursday,
						Friday : alertObj.Friday,
						Saturday : alertObj.Saturday,
						Sunday : alertObj.Sunday
					}).then(function(ref) {
					});
					
					var today = new Date().getDay();
					if($rootScope.alertObj.length==0)
						 $rootScope.alertObj.splice(0,$rootScope.alertObj.length);
					
					switch (today) {
				       case 0:
				    	   if(alertObj.Sunday){
				    		   $rootScope.alertObj.push({
				    			   subjects: $scope.subject,
				    			   times: document.getElementById("alerttime").value
				    		   });
				    	   }
				           break;
				       case 1:
				    	   if(alertObj.Monday){
				    		   $rootScope.alertObj.push({
				    			   subjects: $scope.subject,
				    			   times: document.getElementById("alerttime").value
				    		   });
				    	   }
				           break;
				       case 2:
				    	   if(alertObj.Tuesday){
				    		   $rootScope.alertObj.push({
				    			   subjects: $scope.subject,
				    			   times: document.getElementById("alerttime").value
				    		   });
				    	   }
				           break;
				       case 3:
				    	   if(alertObj.Wednesday){
				    		   $rootScope.alertObj.push({
				    			   subjects: $scope.subject,
				    			   times: document.getElementById("alerttime").value
				    		   });
				    	   }
				           break;
				       case 4:
				    	   if(alertObj.Thursday){
				    		   $rootScope.alertObj.push({
				    			   subjects: $scope.subject,
				    			   times: document.getElementById("alerttime").value
				    		   });
				    	   }
				           break;
				       case 5:
				    	   if(alertObj.Friday){
				    		   $rootScope.alertObj.push({
				    			   subjects: $scope.subject,
				    			   times: document.getElementById("alerttime").value
				    		   });
				    	   }
				           break;
				       case 6:
				    	   if(alertObj.Saturday){
				    		   $rootScope.alertObj.push({
				    			   subjects: $scope.subject,
				    			   times: document.getElementById("alerttime").value
				    		   });
				    	   }
				           break;
				   }
					$scope.subject="";
				}
			} else {
				$location.path('/home');
			}
		} ]);

// Below functionality is used for the Reminders Complete CRUD functionality
homepage.controller('reminderCtrl', [ '$scope', '$location', '$firebaseAuth', '$firebaseObject', '$firebaseArray','$rootScope',
		function($scope, $location, $firebaseAuth, $firebaseObject, $firebaseArray,$rootScope) {

			var fireRef = new Firebase("https://diametrix.firebaseio.com/");
			$scope.authObj = $firebaseAuth(fireRef);
			var authData = $scope.authObj.$getAuth();
			if (null != authData) {

				$scope.reminders = $firebaseArray(new Firebase("https://diametrix.firebaseio.com/" + authData.uid + "/reminders/"));
				console.log($scope.reminders);
				$scope.saveReminder = function() {
					$scope.reminders.$add({
						desc : $scope.desc,
						remtime : document.getElementById("remtime").value,
						remdate : document.getElementById("remdate").value
					});
					$scope.desc=" ";
					$rootScope.title.push($scope.desc);
				    $rootScope.start.push(document.getElementById("remdate").value+"T"+document.getElementById("remtime").value+":00");
				}
			} else {
				$location.path('/home');
			}
			
		} ]);
