/* This Controller is responsible for Posting and Reading All the Bloggs 
 * Below Controller is used for Posting the Blogs
 * */
homepage.controller('postblog', [ '$scope', '$location', '$firebaseAuth', '$firebaseObject', '$firebaseArray', '$rootScope',
		function($scope, $location, $firebaseAuth, $firebaseObject, $firebaseArray, $rootScope) {

			var fireRef = new Firebase("https://diametrix.firebaseio.com/");
			$scope.authObj = $firebaseAuth(fireRef);
			var authData = $scope.authObj.$getAuth();
			if (null != authData) {
				var list = $firebaseArray(new Firebase("https://diametrix.firebaseio.com/blogs/" + authData.uid + "/"));
				$scope.posts = $firebaseArray(new Firebase("https://diametrix.firebaseio.com/blogs/" + authData.uid + "/"));
				$scope.postBlog = function(event) {
					var list = $firebaseArray(new Firebase("https://diametrix.firebaseio.com/blogs/" + authData.uid + "/"));

					list.$add({
						title : $scope.posttitle,
						content : $scope.postcontent,
						likes : 0,
						userid : authData.uid

					}).then(function(ref) {
						$scope.successBlog = "Blog Added Successfully";
						
					}, function(error) {
						$scope.errorBlog = "Please Try Again";
					});
				}
				$("#calendar").fullCalendar({
					header : {
						left : 'prev,next today',
						center : 'title',
						right : 'month,basicWeek,basicDay'
					},
					eventLimit : true, // allow "more" link when too many
					// events
					events : function(start, end, timezone, callback) {
						var events = [];
						for (var i = 0; i < $rootScope.title.length; i++) {
							events.push({
								title : $rootScope.title[i],
								start : $rootScope.start[i]
							});
						}
						callback(events);
					}
				});
			} else {
				$location.path('/home');
			}
		} ]);

// Below Controller Function is used for Fetching All the posted Blogs
homepage.controller('blogCtrl', [ '$scope', '$location', '$firebaseAuth', '$firebaseObject', '$firebaseArray', '$rootScope',
		function($scope, $location, $firebaseAuth, $firebaseObject, $firebaseArray, $rootScope) {

			var fireRef = new Firebase("https://diametrix.firebaseio.com/");
			$scope.authObj = $firebaseAuth(fireRef);
			var authData = $scope.authObj.$getAuth();
			if (null != authData) {
				$scope.posts = $firebaseArray(new Firebase("https://diametrix.firebaseio.com/blogs/"));
				console.log($scope.posts);
				$scope.incrementLikes = function(post, post2) {
					post2.likes += 1;
					post.$save(post2).then(function(ref) {
						console.log(ref);
					}, function(error) {
						console.log("Error:", error);
					});
				}

				$scope.addComments = function(post2, blogid) {
					var obj = $firebaseArray(new Firebase("https://diametrix.firebaseio.com/blogs/" + authData.uid + "/" + blogid + "/comments/"));
					obj.$add({
						comment : "new commensts"
					}).then(function(ref) {
						var id = ref.key();
						console.log("Added record with id: ", id);
					}, function(error) {
						console.log("Error:", error);
					});
				}
				$("#calendar").fullCalendar({
					header : {
						left : 'prev,next today',
						center : 'title',
						right : 'month,basicWeek,basicDay'
					},
					eventLimit : true, // allow "more" link when too many
					// events
					events : function(start, end, timezone, callback) {
						var events = [];
						for (var i = 0; i < $rootScope.title.length; i++) {
							events.push({
								title : $rootScope.title[i],
								start : $rootScope.start[i]
							});
						}
						callback(events);
					}
				});
			} else {
				$location.path('/home');
			}
		} ]);