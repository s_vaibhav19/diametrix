homepage.controller('dashbordCtrl', [
		'$scope',
		'$location',
		'$firebaseAuth',
		'$firebaseObject',
		'$firebaseArray',
		function($scope, $location, $firebaseAuth, $firebaseObject, $firebaseArray) {

			/*
			 * Below is the set of arrays use to show the graphs For Insulin
			 * labelsInsulin seriesInsulin dataInsulin
			 * 
			 * For Sugar labelsSugar seriesSugar dataSugar
			 * 
			 * For Blood Pressure labelsBP seriesBP dataBP
			 */
			$scope.labelsInsulin = [];
			$scope.seriesInsulin = [ 'Insulin' ];
			$scope.dataInsulin = [];

			$scope.labelsSugar = [];
			$scope.seriesSugar = [ 'SugarCount' ];
			$scope.dataSugar = [];

			$scope.labelsBP = [];
			$scope.seriesBP = [ 'HighBp', 'LowBp' ];
			$scope.dataBP = [];

			var bpDataL = [];
			var bpDataH = [];
			var bpDataComplete = [];
			var insulinData = [];
			var sugarCData = [];

			/*
			 * Below varaible are used for donoguth charts sleepinghrs exerhrs
			 * totalHrs
			 * 
			 */
			var sleepinghrs = 0;
			var exerHrs = 0;
			var totalHrs = 0;

			var todaydate = new Date();
			var year = todaydate.getFullYear();

			// setting zero if date is of single digit
			var month = (1 + todaydate.getMonth()).toString();
			month = month.length > 1 ? month : '0' + month;
			var day = todaydate.getDate().toString();
			day = day.length > 1 ? day : '0' + day;

			var fireRef = new Firebase("https://diametrix.firebaseio.com/");
			$scope.authObj = $firebaseAuth(fireRef);
			var authData = $scope.authObj.$getAuth();
			if (null != authData) {

				var trackingData = $firebaseArray(new Firebase("https://diametrix.firebaseio.com/" + authData.uid + "/tracking/" + year + "/" + month
						+ "/"));
				// itterating on data
				trackingData.$loaded().then(function() {
					angular.forEach(trackingData, function(data) {

						insulinData.push(data.Insulin);
						$scope.labelsInsulin.push(data.$id + "/" + month + "/" + year);

						sugarCData.push(data.SugarCount);
						$scope.labelsSugar.push(data.$id + "/" + month + "/" + year);
						if (null != data.BloodPressure) {
							var bp = data.BloodPressure.split("/");
							bpDataH.push(bp[0]);
							bpDataL.push(bp[1]);
						}
						$scope.labelsBP.push(data.$id + "/" + month + "/" + year);
						if (undefined != data.SleepingHrs) {
							sleepinghrs += parseInt(data.SleepingHrs);
						}
						if (undefined != data.ExcersixeHrs) {
							exerHrs += parseInt(data.ExcersixeHrs);
						}
						totalHrs += 24;
					})
					$scope.dataSugar.push(sugarCData);
					$scope.dataInsulin.push(insulinData);
					$scope.dataBP.push(bpDataH, bpDataL);

					$scope.labels = [ "Sleeping Hrs", "Exercise Hrs", "Awake Time" ];
					totalHrs = totalHrs - sleepinghrs - exerHrs;
					$scope.data = [ sleepinghrs, exerHrs, totalHrs ];
				});

			} else {
				$location.path('/home');
			}

		} ]);