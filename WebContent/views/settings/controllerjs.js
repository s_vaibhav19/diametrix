
homepage.controller('profileCtrl',['$scope','$location','$firebaseAuth','$firebaseObject','$rootScope', function($scope, $location, $firebaseAuth, $firebaseObject,$rootScope) {
	
	$scope.regex = "^[0-9]+ ?(\'|ft|cm|meters|feet|in|inches|\")?( *[1-9]+ ?(\"|inches|in|cm)?)?$";
	
	var fireRef = new Firebase("https://diametrix.firebaseio.com/");
	$scope.authObj = $firebaseAuth(fireRef);
	var authData = $scope.authObj.$getAuth();
	
	if (null != authData) {
		var fireRefobj = new Firebase("https://diametrix.firebaseio.com/"+authData.uid+"/profile/");
		var obj = $firebaseObject(fireRefobj);
		obj.$loaded()
		  .then(function(data) {
			  $scope.fname= data.fname;
			  $scope.lname = data.lname;
			  var dates = data.dob;
			  $scope.dob = new Date(dates);
			  $scope.maritalstatus = data.maritalStatus;
			  $scope.height = data.height;
			  $scope.weight = data.weight;
			  $scope.sex = data.sex;
			  $scope.bloodgrp = data.bloodgrp;
			  $scope.rptshare = data.rptShare;
		  })
		  .catch(function(error) {
		    console.error("Error:", error);
		  });

	$scope.updateProfile = function(event) {
		
		  var fbobj = $firebaseObject(fireRefobj); 
		  fbobj.fname = $scope.fname;
		  fbobj.lname = $scope.lname;
		  fbobj.dob = $scope.dob;
		  fbobj.maritalStatus = $scope.maritalstatus;
		  fbobj.height = $scope.height;
		  fbobj.weight = $scope.weight;
		  fbobj.sex = $scope.sex;
		  fbobj.bloodgrp = $scope.bloodgrp;
		  
		  fbobj.rptShare = $scope.rptshare;
		  
			fbobj.$save().then(function(ref) {
				console.log(ref);

			}, function(error) {
				console.log("Error:", error);
			});
			
			var saveShare = $firebaseObject(new Firebase("https://diametrix.firebaseio.com/rptshare/"+authData.uid+"/"));
			saveShare.userid=authData.uid;
			saveShare.email=$scope.rptshare;
			saveShare.$save().then(function(ref) {
				$scope.successProfil="Profile Updated Successfully";
			}, function(error) {
				$scope.errorProfil ="Please Try Again";
			});
	}
	
	$("#calendar").fullCalendar({
		header : {
			left : 'prev,next today',
			center : 'title',
			right : 'month,basicWeek,basicDay'
		},
		eventLimit : true, // allow "more" link when too many events
		events : function(start, end, timezone, callback) {
			var events = [];
			for (var i = 0; i < $rootScope.title.length; i++) {
				events.push({
					title : $rootScope.title[i],
					start : $rootScope.start[i]
				});
			}
			callback(events);
		}
	});
	} else {
		  $location.path('/home');
		}
}]);

// updating the default settings for uses checkbox
homepage.controller('advanceCtrl',['$scope','$location','$firebaseAuth','$firebaseObject','$rootScope', function($scope, $location, $firebaseAuth, $firebaseObject,$rootScope) {
	var fireRef = new Firebase("https://diametrix.firebaseio.com");
	$scope.authObj = $firebaseAuth(fireRef);
	var authData = $scope.authObj.$getAuth();
	if(null != authData){
	var fireRefobj = new Firebase("https://diametrix.firebaseio.com/"+authData.uid+"/advsetting/");
	$scope.advsetting = $firebaseObject(new Firebase("https://diametrix.firebaseio.com/"+authData.uid+"/advsetting/"));
	
	$("#calendar").fullCalendar({
		header : {
			left : 'prev,next today',
			center : 'title',
			right : 'month,basicWeek,basicDay'
		},
		eventLimit : true, // allow "more" link when too many events
		events : function(start, end, timezone, callback) {
			var events = [];
			for (var i = 0; i < $rootScope.title.length; i++) {
				events.push({
					title : $rootScope.title[i],
					start : $rootScope.start[i]
				});
			}
			callback(events);
		}
	});
	} else {
		  $location.path('/home');
		}
}]);

// Changed Password Functionality inside profile setting needs old password and new password  
homepage.controller('changepsd',['$scope','$location','$firebaseAuth','$firebaseObject', function($scope, $location, $firebaseAuth, $firebaseObject) {
	var fireRef = new Firebase("https://diametrix.firebaseio.com");
	$scope.authObj = $firebaseAuth(fireRef);
	var authData = $scope.authObj.$getAuth();
	if(null != authData){
	$scope.changePassword = function(event){
		fireRef.changePassword({
			  email: authData.auth.token.email,
			  oldPassword: $scope.oldpsd,
			  newPassword: $scope.newpsd
			}, function(error) {
			  if (error) {
			    switch (error.code) {
			      case "INVALID_PASSWORD":
			        $scope.chgerrmessage="Invalid Password Try Again";
			        
			        break;
			      case "INVALID_USER":
			    	  $scope.chgerrmessage="Erro Please Try Again";
			    	  
			        break;
			      default:
			    	  $scope.chgerrmessage="Error changing password";
			    }
			  } else {
				  
				  $scope.chgsccmessage="User password changed successfully!";
			  }
			});
	}
	} else {
		  $location.path('/home');
		}
}]);
