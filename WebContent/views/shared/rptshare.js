homepage.controller('rptshareCtrl', [
		'$scope',
		'$location',
		'$firebaseAuth',
		'$firebaseObject',
		'$firebaseArray',
		function($scope, $location, $firebaseAuth, $firebaseObject, $firebaseArray) {
			var fireRef = new Firebase("https://diametrix.firebaseio.com/");
			$scope.authObj = $firebaseAuth(fireRef);
			var authData = $scope.authObj.$getAuth();
			if(null != authData){
			var rptdata = $firebaseArray(new Firebase("https://diametrix.firebaseio.com/rptshare/"));
			var emailCheck = authData.password.email;
			rptdata.$loaded().then(
					function() {
						angular.forEach(rptdata, function(data) {
							if (data.email == emailCheck) {

								$scope.labelsInsulin = [];
								$scope.seriesInsulin = [ 'Insulin' ];
								$scope.dataInsulin = [];

								$scope.labelsSugar = [];
								$scope.seriesSugar = [ 'SugarCount' ];
								$scope.dataSugar = [];

								$scope.labelsBP = [];
								$scope.seriesBP = [ 'HighBp', 'LowBp' ];
								$scope.dataBP = [];

								var bpDataL = [];
								var bpDataH = [];
								var bpDataComplete = [];
								var insulinData = [];
								var sugarCData = [];
								var sleepinghrs = 0;
								var exerHrs = 0;
								var totalHrs = 0;

								var todaydate = new Date();
								var year = todaydate.getFullYear();

								var month = (1 + todaydate.getMonth()).toString();
								month = month.length > 1 ? month : '0' + month;
								var day = todaydate.getDate().toString();
								day = day.length > 1 ? day : '0' + day;

								var fireRef = new Firebase("https://diametrix.firebaseio.com/");
								$scope.authObj = $firebaseAuth(fireRef);
								var authData = $scope.authObj.$getAuth();

								var trackingData = $firebaseArray(new Firebase("https://diametrix.firebaseio.com/" + data.userid + "/tracking/"
										+ year + "/" + month + "/"));

								trackingData.$loaded().then(function() {
									angular.forEach(trackingData, function(data) {

										insulinData.push(data.Insulin);
										$scope.labelsInsulin.push(data.$id + "/" + month + "/" + year);

										sugarCData.push(data.SugarCount);
										$scope.labelsSugar.push(data.$id + "/" + month + "/" + year);

										if (null != data.BloodPressure) {
											var bp = data.BloodPressure.split("/");
											bpDataH.push(bp[0]);
											bpDataL.push(bp[1]);
										}
										$scope.labelsBP.push(data.$id + "/" + month + "/" + year);
										if (undefined != data.SleepingHrs) {
											sleepinghrs += parseInt(data.SleepingHrs);
										}
										if (undefined != data.ExcersixeHrs) {
											exerHrs += parseInt(data.ExcersixeHrs);
										}
										totalHrs += 24;
									})
									$scope.dataSugar.push(sugarCData);
									$scope.dataInsulin.push(insulinData);
									$scope.dataBP.push(bpDataH, bpDataL);

									$scope.labels = [ "Sleeping Hrs", "Exercise Hrs", "Awake Time" ];
									totalHrs = totalHrs - sleepinghrs - exerHrs;
									$scope.data = [ sleepinghrs, exerHrs, totalHrs ];
								});
							}
						})
					});
			}else{
				$location.path('/home');
			}
		} ]);