/* This Controller is responsible for fetching the advance settings values and form the 3-way data bindings for tracking page */
homepage.controller('trackCtrl', [
		'$scope',
		'$location',
		'$firebaseAuth',
		'$firebaseObject',
		'$firebaseArray',
		'$rootScope',
		function($scope, $location, $firebaseAuth, $firebaseObject, $firebaseArray, $rootScope) {

			var todayDate = new Date();
			var fireRef = new Firebase("https://diametrix.firebaseio.com/");
			$scope.authObj = $firebaseAuth(fireRef);
			var authData = $scope.authObj.$getAuth();

			if (null != authData) {
				$scope.trackingData = $firebaseObject(new Firebase("https://diametrix.firebaseio.com/" + authData.uid + "/tracking/"
						+ getFormattedDate(todayDate) + "/"));
				$scope.advsettigns = $firebaseObject(new Firebase("https://diametrix.firebaseio.com/" + authData.uid + "/advsetting/"));
				$scope.crabs = $firebaseObject(new Firebase("https://diametrix.firebaseio.com/crabs"));

				$("#calendar").fullCalendar({
					header : {
						left : 'prev,next today',
						center : 'title',
						right : 'month,basicWeek,basicDay'
					},
					eventLimit : true, // allow "more" link when too many
					// events
					events : function(start, end, timezone, callback) {
						var events = [];
						for (var i = 0; i < $rootScope.title.length; i++) {
							events.push({
								title : $rootScope.title[i],
								start : $rootScope.start[i]
							});
						}
						callback(events);
					}
				});
			} else {
				$location.path('/home');
			}
		} ]);

function getFormattedDate(date) {
	var year = date.getFullYear();
	var month = (1 + date.getMonth()).toString();
	month = month.length > 1 ? month : '0' + month;
	var day = date.getDate().toString();
	day = day.length > 1 ? day : '0' + day;
	return year + '/' + month + '/' + day;
}